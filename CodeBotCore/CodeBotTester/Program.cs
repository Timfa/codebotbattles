﻿using CodeBotCore;
using CodeBotCore.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeBotTester
{
    class Program
    {
        static void Main(string[] args)
        {
            CodeBotBattlesGame game = new CodeBotBattlesGame();
            game.SetupBoard(BattleMap.DefaultChallengeMaps[0]);
            
            Bot bot = new Bot(game, 4, 6, Bot.Directions.UP, Bot.Teams.BOTTOM, 1);
            bot.LoadScript("LTURN LOOP MOVE RTURN SCAN REGDEC IFZERO FIRE END REGDEC REGDEC IFZERO FIRE END LTURN DISTANCE IFZERO BREAK END END LTURN");

            while (true)
            { 
                BoardObject[,] state = game.CurrentBoardState;

                for (int y = 0; y < game.BoardHeight; y++)
                {
                    for (int x = 0; x < game.BoardWidth; x++)
                    {
                        string c = " ";

                        if (state[x, y] != null)
                        {
                            if (state[x, y] is Wall)
                            {
                                c = "█";
                            }

                            if (state[x, y] is DestructibleWall)
                            {
                                c = "□";
                            }

                            if (state[x, y] is Bot)
                            {
                                Bot sBot = (Bot)state[x, y];
                                c = "X";

                                switch (sBot.Direction)
                                {
                                    case Bot.Directions.DOWN:
                                        c = "v";
                                        break;

                                    case Bot.Directions.UP:
                                        c = "^";
                                        break;

                                    case Bot.Directions.RIGHT:
                                        c = ">";
                                        break;

                                    case Bot.Directions.LEFT:
                                        c = "<";
                                        break;
                                }

                                if(sBot.Firing)
                                {
                                    c = "+";
                                }

                                if(sBot.CurrentAction == ValidInstructions.SCAN)
                                {
                                    c = "S";
                                }

                                if(sBot.CurrentAction == ValidInstructions.DISTANCE)
                                {
                                    c = "D";
                                }
                            }

                            if(state[x, y].DestroyedThisTick)
                            {
                                c = "@";
                            }
                        }

                        Console.Write(c);
                    }

                    Console.WriteLine("");
                }
                
                Thread.Sleep(500);
                game.GameTick();
                Console.Clear();
            }
        }
    }
}
