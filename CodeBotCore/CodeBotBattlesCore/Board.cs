﻿using CodeBotCore.Objects;
using System;
using System.Collections.Generic;

namespace CodeBotCore
{
    internal class Board
    {
        internal int Width { get; private set; }
        internal int Height { get; private set; }

        internal CodeBotBattlesGame game;

        internal List<BoardObject> boardObjects = new List<BoardObject>();
        private Wall voidWall;
        
        internal BoardObject[,] CurrentBoardState
        {
            get
            {
                BoardObject[,] state = new BoardObject[Width, Height];

                for(int x = 0; x < Width; x++)
                {
                    for(int y = 0; y < Height; y++)
                    {
                        BoardObject found = GetBoardObjectAt(x, y);

                        if (found != null)
                            state[x, y] = found.Destroyed ? null : found;
                    }
                }

                return state;
            }
        }

        internal void Reset()
        {
            foreach(BoardObject bo in boardObjects)
            {
                bo.Reset();
            }
        }

        internal Board(CodeBotBattlesGame game, int width, int height)
        {
            this.game = game;
            Width = width;
            Height = height;
        }

        internal void Init()
        {
            voidWall = new Wall(game, -10, -10);
        }

        internal void GameTick()
        {
            boardObjects.Sort();

            for(int i = 0; i < boardObjects.Count; i++)
            {
                boardObjects[i].OnTick();
            }
        }

        internal BoardObject GetBoardObjectAt(int x, int y) //Returns a wall when out of bounds
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return voidWall;

            return boardObjects.Find(o => o.X == x && o.Y == y && !o.Destroyed); //Will not work with two objects at the same position, though this should not be possible anyway.
        }

        internal void AddObjectToBoard(BoardObject bObject)
        {
            if (boardObjects.Find(o => o.X == bObject.X && o.Y == bObject.Y) != null)
                throw new InvalidOperationException("Tile already occupied! [" + bObject.X + ", " + bObject.Y + "]");

            boardObjects.Add(bObject);
        }
    }
}
