﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore.Exceptions
{
    class BoardNotSetupException : Exception
    {
        public override string Message
        {
            get
            {
                return "The board has not yet been set up!";
            }
        }
    }
}
