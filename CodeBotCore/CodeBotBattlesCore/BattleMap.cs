﻿using CodeBotCore.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CodeBotCore.Objects.Bot;

namespace CodeBotCore
{
    public class BattleMap
    {
        public string Name { get; internal set; }
        public int Width { get; internal set; }
        public int Height { get; internal set; }

        public int PlaceableRows { get; internal set; }

        public Tile[] Walls { get; internal set; }
        public Tile[] DestructibleWalls { get; internal set; }

        public PlacedBot[] Bots { get; internal set; }

        internal static List<BattleMap> ExtraBattleMaps = new List<BattleMap>();
        internal static List<BattleMap> ExtraChallengeMaps = new List<BattleMap>();

        internal BattleMap()
        {

        }

        public void RotateCCW()
        {
            foreach (Tile t in Walls)
            {
                int newX = t.Y;
                t.Y = (Height - 1) - t.X;
                t.X = newX;
            }

            foreach (Tile t in DestructibleWalls)
            {
                int newX = t.Y;
                t.Y = (Height - 1) - t.X;
                t.X = newX;
            }

            foreach (PlacedBot t in Bots)
            {
                int newX = t.Y;
                t.Y = (Height - 1) - t.X;
                t.X = newX;

                switch (t.Orientation)
                {
                    case Directions.DOWN:
                        t.Orientation = Directions.RIGHT;
                        break;

                    case Directions.RIGHT:
                        t.Orientation = Directions.UP;
                        break;

                    case Directions.LEFT:
                        t.Orientation = Directions.DOWN;
                        break;

                    case Directions.UP:
                        t.Orientation = Directions.LEFT;
                        break;
                }

                switch (t.Team)
                {
                    case Teams.BOTTOM:
                        t.Team = Teams.RIGHT;
                        break;

                    case Teams.RIGHT:
                        t.Team = Teams.TOP;
                        break;

                    case Teams.LEFT:
                        t.Team = Teams.BOTTOM;
                        break;

                    case Teams.TOP:
                        t.Team = Teams.LEFT;
                        break;
                }
            }
        }

        public void RotateCW()
        {
            foreach (Tile t in Walls)
            {
                int newX = (Width - 1) - t.Y;
                t.Y = t.X;
                t.X = newX;
            }

            foreach (Tile t in DestructibleWalls)
            {
                int newX = (Width - 1) - t.Y;
                t.Y = t.X;
                t.X = newX;
            }

            foreach (PlacedBot t in Bots)
            {
                int newX = (Width - 1) - t.Y;
                t.Y = t.X;
                t.X = newX;

                switch (t.Orientation)
                {
                    case Directions.DOWN:
                        t.Orientation = Directions.LEFT;
                        break;

                    case Directions.RIGHT:
                        t.Orientation = Directions.DOWN;
                        break;

                    case Directions.LEFT:
                        t.Orientation = Directions.UP;
                        break;

                    case Directions.UP:
                        t.Orientation = Directions.RIGHT;
                        break;
                }

                switch (t.Team)
                {
                    case Teams.BOTTOM:
                        t.Team = Teams.LEFT;
                        break;

                    case Teams.RIGHT:
                        t.Team = Teams.BOTTOM;
                        break;

                    case Teams.LEFT:
                        t.Team = Teams.TOP;
                        break;

                    case Teams.TOP:
                        t.Team = Teams.RIGHT;
                        break;
                }
            }
        }

        public BattleMap(string name, int width, int height, int placeableRows, Tile[] walls, Tile[] destructibleWalls, PlacedBot[] bots = null)
        {
            Name = name;
            Width = width;
            Height = height;
            PlaceableRows = placeableRows;
            Walls = walls;
            DestructibleWalls = destructibleWalls;

            if (bots == null)
                Bots = new PlacedBot[0];
            else
                Bots = bots;
        }

        public static BattleMap FromJson(string json)
        {
            return JsonConvert.DeserializeObject<BattleMap>(json);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public class Tile
        {
            public int X, Y;

            public Tile(int x, int y)
            {
                X = x;
                Y = y;
            }

            public Tile()
            {

            }
        }

        public class PlacedBot
        {
            public int X, Y;
            public Bot.Teams Team;
            public Bot.Directions Orientation;
            public string Script;
            public int MemorySize;

            public PlacedBot(int x, int y, Bot.Teams team, Bot.Directions orientation, int memorySize, string script = "WAIT")
            {
                X = x;
                Y = y;
                Script = script;

                Team = team;
                Orientation = orientation;
                MemorySize = memorySize;
            }

            public PlacedBot()
            {
                Script = "WAIT";
                MemorySize = 1;
            }
        }

        public static void RegisterBattleMap(BattleMap toRegister)
        {
            ExtraBattleMaps.Add(toRegister);
        }

        public static void RegisterChallengeMap(BattleMap toRegister)
        {
            ExtraChallengeMaps.Add(toRegister);
        }

        public static BattleMap[] AllBattleMaps
        {
            get
            {
                List<BattleMap> maps = new List<BattleMap>();

                maps.AddRange(DefaultBattleMaps);
                maps.AddRange(ExtraBattleMaps);

                return maps.ToArray();
            }
        }

        public static BattleMap[] AllChallengeMaps
        {
            get
            {
                List<BattleMap> maps = new List<BattleMap>();

                maps.AddRange(DefaultBattleMaps);
                maps.AddRange(ExtraBattleMaps);

                return maps.ToArray();
            }
        }

        public static BattleMap GetBattleMapByName(string name)
        {
            return AllBattleMaps.FirstOrDefault(o => o.Name == name);
        }

        public static BattleMap GetChallengeMapByName(string name)
        {
            return AllChallengeMaps.FirstOrDefault(o => o.Name == name);
        }

        public static BattleMap[] DefaultBattleMaps = new BattleMap[]
        {
            new BattleMap("Empty Square", 15, 15, 3, new Tile[]{}, new Tile[]{}),
            new BattleMap("Small Block", 7, 7, 2, new Tile[]{}, new Tile[]{new Tile(2, 2), new Tile(3, 2), new Tile(4, 2), new Tile(2, 3), new Tile(3, 3), new Tile(4, 3), new Tile(2, 4), new Tile(3, 4), new Tile(4, 4)})
        };

        public static BattleMap[] DefaultChallengeMaps = new BattleMap[]
        {
            new BattleMap("Small Block", 7, 7, 2, new Tile[]{}, new Tile[]{new Tile(2, 2), new Tile(3, 2), new Tile(4, 2), new Tile(2, 3), new Tile(3, 3), new Tile(4, 3), new Tile(2, 4), new Tile(3, 4), new Tile(4, 4)}, new PlacedBot[]{ new PlacedBot(1, 1, Bot.Teams.TOP, Bot.Directions.RIGHT, 1, "LOOP MOVE RTURN SCAN REGDEC IFZERO FIRE END REGDEC REGDEC IFZERO FIRE END LTURN DISTANCE IFZERO BREAK END END RTURN") })
        };
    }
}
