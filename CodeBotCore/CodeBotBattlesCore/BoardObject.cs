﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore
{
    public abstract class BoardObject : IComparable
    {
        private static int idCounter = 0;
        public int ObjectID { get; private set; }

        public int X { get; internal set; }
        public int Y { get; internal set; }
        private int oX, oY;
        public bool Destroyed { get; private set; }
        public bool DestroyedThisTick = false;

        internal virtual int TickOrder
        {
            get
            {
                return 0;
            }
        }

        protected CodeBotBattlesGame game;

        public BoardObject(CodeBotBattlesGame game, int x, int y)
        {
            idCounter++;
            ObjectID = idCounter;

            this.game = game;
            Destroyed = false;

            X = x;
            Y = y;

            oX = x;
            oY = y;

            game.Board.AddObjectToBoard(this);
        }

        internal void Destroy()
        {
            DestroyedThisTick = true;
        }

        internal virtual void Reset()
        {
            X = oX;
            Y = oY;
            Destroyed = false;
        }

        internal virtual void OnTick()
        {
            if (DestroyedThisTick)
                Destroyed = true;
        }

        public int CompareTo(object obj)
        {
            if (obj is BoardObject)
            {
                return TickOrder.CompareTo(((BoardObject)obj).TickOrder);
            }

            return 0;
        }
    }
}
