﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore.Objects
{
    public class Wall : BoardObject
    {
        public Wall(CodeBotBattlesGame game, int x, int y) : base(game, x, y)
        {
        }
    }
}
