﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using static CodeBotCore.Objects.Bot;

namespace CodeBotCore.Objects
{
    class BotScriptExecutor
    {
        public ValidInstructions[] Instructions { get; private set; }
        private int instructionIndex = 0;
        public const int MaxInstructionsPerTick = 1000;
        private static Random random = new Random();

        private Stack<int> starts = new Stack<int>();
        
        internal static ValidInstructions[] BlockingInstructions =
        {
            ValidInstructions.MOVE,
            ValidInstructions.LTURN,
            ValidInstructions.RTURN,
            ValidInstructions.FIRE,
            ValidInstructions.SCAN,
            ValidInstructions.DISTANCE,
            ValidInstructions.WAIT,
            ValidInstructions.TRANSMIT,
            ValidInstructions.RECEIVE
        };

        internal static Queue<byte> WaitingSend = new Queue<byte>();
        internal static Queue<BotScriptExecutor> WaitingReceive = new Queue<BotScriptExecutor>();

        private Bot bot;

        public BotScriptExecutor(Bot owner)
        {
            bot = owner;
            Instructions = new ValidInstructions[] { ValidInstructions.WAIT };
        }

        internal static void ResetWaitingQueues()
        {
            WaitingSend = new Queue<byte>();

            while (WaitingReceive.Count > 0)
                WaitingReceive.Dequeue().Receive(0x00);

            WaitingReceive = new Queue<BotScriptExecutor>();
        }

        internal void Receive(byte data)
        {
            bot.Memory.Register = data;
        }

        internal void ExecuteToNextTick()
        {
            bool blockerEncountered = false;
            int executedInstructions = 0;
            bool shouldExplode = true;

            //The first action performed this tick should always be a blocker, which is the only kind a rendering engine will care about.
            bot.CurrentAction = Instructions[instructionIndex]; 

            while (!blockerEncountered && executedInstructions < MaxInstructionsPerTick)
            {
                executedInstructions++;
                
                switch(Instructions[instructionIndex])
                {
                        //BLOCKERS
                    case ValidInstructions.MOVE:
                        bot.MoveForwards();
                        instructionIndex++;
                        break;

                    case ValidInstructions.LTURN:
                        bot.TurnLeft();
                        instructionIndex++;
                        break;

                    case ValidInstructions.RTURN:
                        bot.TurnRight();
                        instructionIndex++;
                        break;

                    case ValidInstructions.FIRE:
                        bot.Fire();
                        instructionIndex++;
                        break;

                    case ValidInstructions.SCAN:
                        ScanResult scanResult = bot.Scan();

                        if(scanResult.Detected == null)
                        {
                            bot.Memory.Register = 0x00; //nothing
                        }
                        else if(scanResult.Detected is Bot)
                        {
                            if (((Bot)scanResult.Detected).Team == bot.Team)
                                bot.Memory.Register = 0x02; //friend
                            else
                                bot.Memory.Register = 0x01; //enemy
                        }
                        else if (scanResult.Detected is DestructibleWall)
                        {
                            bot.Memory.Register = 0x03;
                        }
                        else if (scanResult.Detected is Wall)
                        {
                            bot.Memory.Register = 0x04;
                        }

                        instructionIndex++;
                        break;

                    case ValidInstructions.DISTANCE:
                        ScanResult distResult = bot.Scan();
                        bot.Memory.Register = distResult.Distance;
                        instructionIndex++;
                        break;

                    case ValidInstructions.WAIT:
                        //Do nothing.
                        instructionIndex++;
                        break;

                    case ValidInstructions.TRANSMIT:
                        if(WaitingReceive.Count > 0)
                        {
                            WaitingReceive.Dequeue().Receive(bot.Memory.Register);
                        }
                        else
                        {
                            WaitingSend.Enqueue(bot.Memory.Register);
                        }
                        break;

                    case ValidInstructions.RECEIVE:
                        if(WaitingSend.Count > 0)
                        {
                            Receive(WaitingSend.Dequeue());
                        }
                        else
                        {
                            WaitingReceive.Enqueue(this);
                        }
                        break;

                    //NON BLOCKERS

                    case ValidInstructions.MEMLEFT:
                        bot.Memory.PointerLeft();
                        instructionIndex++;
                        break;

                    case ValidInstructions.MEMRIGHT:
                        bot.Memory.PointerRight();
                        instructionIndex++;
                        break;

                    case ValidInstructions.REGDEC:
                        bot.Memory.ByteDown();
                        instructionIndex++;
                        break;

                    case ValidInstructions.REGINC:
                        bot.Memory.ByteUp();
                        instructionIndex++;
                        break;

                    case ValidInstructions.RAND:
                        byte[] oneByte = new byte[1];
                        random.NextBytes(oneByte);

                        bot.Memory.Register = oneByte[0];
                        instructionIndex++;
                        break;

                    case ValidInstructions.DIRECTION:
                        bot.Memory.CurrentByte = (byte)((int)bot.Direction + (int)bot.Team % 4);
                        instructionIndex++;
                        break;

                    case ValidInstructions.REGADD:
                        bot.Memory.RegisterAdd();
                        break;

                    case ValidInstructions.REGSUB:
                        bot.Memory.RegisterSubtract();
                        break;

                    case ValidInstructions.REGSTORE:
                        bot.Memory.RegisterStore();
                        break;

                    case ValidInstructions.REGLOAD:
                        bot.Memory.RegisterLoad();
                        break;
                }
                
                if (instructionIndex >= Instructions.Length)
                    instructionIndex = 0; //loop to start when reaching the end.

                ValidInstructions currInstruction = Instructions[instructionIndex];

                switch (currInstruction)
                {
                    case ValidInstructions.IFNOTZERO:
                        if(bot.Memory.Register == 0x00)
                        {
                            SkipToEND(); 
                        }
                        else
                        {
                            starts.Push(instructionIndex); //Add the loop index to the starts
                            instructionIndex++; //skip one ahead, this one is already handled.
                        }
                        break;

                    case ValidInstructions.IFZERO:
                        if (bot.Memory.Register != 0x00)
                        {
                            SkipToEND(); 
                        }
                        else
                        {
                            starts.Push(instructionIndex); //Add the loop index to the starts
                            instructionIndex++; //skip one ahead, this one is already handled.
                        }
                        break;

                    case ValidInstructions.BREAK:
                        bool whileFound = false;

                        while (!whileFound && starts.Count > 0)
                        {
                            instructionIndex = starts.Pop(); //Remove the loop inex from starts as we're not using it anymore

                            if (Instructions[instructionIndex] == ValidInstructions.LOOP)
                                whileFound = true;
                        }
                        SkipToEND();
                        break;

                    case ValidInstructions.LOOP:
                        starts.Push(instructionIndex); //Add the loop index to the starts
                        instructionIndex++; //skip one ahead, this one is already handled.
                        break;

                    case ValidInstructions.END:
                        int potentialLoop = starts.Peek();

                        if (Instructions[potentialLoop] == ValidInstructions.LOOP)
                        {
                            instructionIndex = potentialLoop + 1; //move the instructionindex one ahead of this loop's start. Don't change starts as it's good as it is.
                        }
                        else //it's an IF
                        {
                            starts.Pop();
                            instructionIndex++; //skip one ahead, this one is already handled.
                        }
                        break;
                }

                if (instructionIndex >= Instructions.Length)
                    instructionIndex = 0; //loop to start when reaching the end.

                //When reaching a blocker, stop executing for now and tell the self-destruct to relax. Blocker will be the first to be executed in the next iteration and will not be executed now.
                for (int i = 0; i < BlockingInstructions.Length; i++)
                {
                    if (BlockingInstructions[i] == Instructions[instructionIndex])
                    {
                        shouldExplode = false;
                        blockerEncountered = true;
                    }
                }
            }

            if(shouldExplode)
            {
                bot.Error("Timeout!");
            }
        }

        private void SkipToEND()
        {
            int ignores = 0;
            bool done = false;

            while(!done)
            {
                instructionIndex++;

                if(instructionIndex >= Instructions.Length)
                {
                    done = true;
                    bot.Error("Syntax error: LOOP missing an END");
                }

                if (Instructions[instructionIndex] == ValidInstructions.IFNOTZERO)
                    ignores++;

                if (Instructions[instructionIndex] == ValidInstructions.IFZERO)
                    ignores++;

                if (Instructions[instructionIndex] == ValidInstructions.LOOP)
                    ignores++;

                if (Instructions[instructionIndex] == ValidInstructions.END)
                {
                    if(ignores <= 0)
                    {
                        done = true;
                        instructionIndex++; //Skip to one ahead of the matching END.
                    }
                    else
                        ignores--;
                }
            }
        }

        private struct IsValidInstructionResult
        {
            public bool IsValid;
            public ValidInstructions Instruction;
        }

        private IsValidInstructionResult IsValidInstruction(string instruction)
        {
            IsValidInstructionResult result = new IsValidInstructionResult();

            for (int i = 0; i < Enum.GetNames(typeof(ValidInstructions)).Length; i++)
            {
                if (instruction == Enum.GetNames(typeof(ValidInstructions))[i])
                {
                    result.IsValid = true;
                    result.Instruction = (ValidInstructions)i;
                }
            }

            return result;
        }

        public void Reset()
        {
            instructionIndex = 0;
        }
        
        public void LoadScript(string script)
        {
            script = Regex.Replace(script, @" ", @" ");
            script = Regex.Replace(script, @"	", @" ");
            script = Regex.Replace(script, @" {2,}", @" ");
            string[] words = Regex.Split(script, @" ");

            List<string> wordsList = new List<string>();
            wordsList.AddRange(words);
            List<ValidInstructions> instructions = new List<ValidInstructions>();

            for (int i = 0; i < wordsList.Count; i++)
            {
                wordsList[i] = wordsList[i].Trim();

                IsValidInstructionResult result = IsValidInstruction(wordsList[i]);

                if (result.IsValid)
                {
                    instructions.Add(result.Instruction);
                }
            }

            Instructions = instructions.ToArray();
        }
    }
}
