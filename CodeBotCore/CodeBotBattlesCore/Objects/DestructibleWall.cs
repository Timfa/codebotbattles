﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore.Objects
{
    public class DestructibleWall : BoardObject
    {
        public DestructibleWall(CodeBotBattlesGame game, int x, int y) : base(game, x, y)
        {
        }
    }
}
