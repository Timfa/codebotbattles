﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore.Objects
{
    public class BotMemory
    {
        private byte register = 0x00;
        private byte[] memory = new byte[0];
        private int pointer = 0;

        public BotMemory(int memorySize)
        {
            if (memorySize < 0)
                memorySize = 0;

            if (memorySize > 1000)
                memorySize = 1000;

            memory = new byte[memorySize];
        }

        public int Size
        {
            get
            {
                return memory.Length;
            }
        }

        public byte Register
        {
            get
            {
                return register;
            }

            internal set
            {
                register = value;
            }
        }

        public string RegisterString
        {
            get
            {
                return "0x" + register.ToString("X2");
            }
        }

        public byte CurrentByte
        {
            get
            {
                return memory[pointer];
            }

            internal set
            {
                memory[pointer] = value;
            }
        }

        public string CurrentByteString
        {
            get
            {
                return "0x" + CurrentByte.ToString("X2");
            }
        }

        public void PointerRight()
        {
            pointer++;

            if (pointer >= memory.Length)
                pointer = memory.Length - 1;
        }

        public void PointerLeft()
        {
            pointer--;

            if (pointer < 0)
                pointer = 0;
        }

        public void ByteUp()
        {
            if (Register < 0xFF)
                Register++;
            else
                Register = 0x00;
        }

        public void ByteDown()
        {
            if (Register > 0x00)
                Register--;
            else
                Register = 0xFF;
        }

        public void RegisterAdd()
        {
            int mem = CurrentByte;
            int toAdd = Register;

            mem += toAdd;
            mem %= 255;

            register = (byte)mem;
        }

        public void RegisterSubtract()
        {
            int mem = CurrentByte;
            int reg = Register;

            reg -= reg;

            if (reg < 0)
                reg += 255;

            register = (byte)reg;
        }

        public void RegisterStore()
        {
            CurrentByte = register;
        }

        public void RegisterLoad()
        {
            register = CurrentByte;
        }
    }
}
