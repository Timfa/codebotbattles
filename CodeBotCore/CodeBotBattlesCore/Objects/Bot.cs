﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using static CodeBotCore.Objects.BotScriptExecutor;

namespace CodeBotCore.Objects
{
    public class Bot : BoardObject
    {
        public ValidInstructions CurrentAction { get; internal set; }
        public BotMemory Memory { get; private set; }

        public Directions Direction { get; internal set; }
        private Directions oDirection;
        private BotScriptExecutor scriptExecutor;
        public Teams Team { get; private set; }
        public enum Teams { BOTTOM, RIGHT, TOP, LEFT }

        public bool Firing { get; private set; }

        internal override int TickOrder
        {
            get
            {
                return scriptExecutor.Instructions.Length + Memory.Size;
            }
        }

        public Bot(CodeBotBattlesGame game, int x, int y, Directions initialDirection, Teams team, int memorySize) : base(game, x, y)
        {
            Memory = new BotMemory(memorySize);
            Direction = initialDirection;
            oDirection = Direction;
            scriptExecutor = new BotScriptExecutor(this);
            Team = team;
        }

        internal void Error(string error = "Unknown Error")
        {
            //Error in script!
            Destroy();
        }

        internal override void Reset()
        {
            base.Reset();
            Memory = new BotMemory(Memory.Size);
            Direction = oDirection;
            scriptExecutor.Reset();
        }

        public void LoadScript(string script)
        {
            scriptExecutor.LoadScript(script);
        }
        
        public void LoadScriptFromFile(string path)
        {

        }

        internal override void OnTick()
        {
            Firing = false;
            base.OnTick();

            scriptExecutor.ExecuteToNextTick();
        }

        public enum Directions
        {
            UP, RIGHT, DOWN, LEFT
        }

        internal void MoveForwards()
        {
            if (Destroyed)
                return;

            switch (Direction)
            {
                case Directions.DOWN:
                    if (game.Board.GetBoardObjectAt(X, Y + 1) == null)
                        Y++;
                    break;

                case Directions.RIGHT:
                    if (game.Board.GetBoardObjectAt(X + 1, Y) == null)
                        X++;
                    break;

                case Directions.LEFT:
                    if (game.Board.GetBoardObjectAt(X - 1, Y) == null)
                        X--;
                    break;

                case Directions.UP:
                    if (game.Board.GetBoardObjectAt(X, Y - 1) == null)
                        Y--;
                    break;
            }
        }

        internal void TurnLeft()
        {
            switch (Direction)
            {
                case Directions.DOWN:
                    Direction = Directions.RIGHT;
                    break;

                case Directions.RIGHT:
                    Direction = Directions.UP;
                    break;

                case Directions.LEFT:
                    Direction = Directions.DOWN;
                    break;

                case Directions.UP:
                    Direction = Directions.LEFT;
                    break;
            }
        }

        internal void TurnRight()
        {
            switch (Direction)
            {
                case Directions.DOWN:
                    Direction = Directions.LEFT;
                    break;

                case Directions.RIGHT:
                    Direction = Directions.DOWN;
                    break;

                case Directions.LEFT:
                    Direction = Directions.UP;
                    break;

                case Directions.UP:
                    Direction = Directions.RIGHT;
                    break;
            }
        }

        internal void Fire()
        {
            if (Destroyed)
                return;

            Firing = true;

            ScanResult result = Scan();
            
            if(result.Detected is Bot || result.Detected is DestructibleWall)
            {
                result.Detected.Destroy();
            }
        }

        internal ScanResult Scan()
        {
            bool found = false;

            int x = X;
            int y = Y;
            ScanResult result = new ScanResult();

            while(!found)
            {
                switch (Direction)
                {
                    case Directions.DOWN:
                            y++;
                        break;

                    case Directions.RIGHT:
                            x++;
                        break;

                    case Directions.LEFT:
                            x--;
                        break;

                    case Directions.UP:
                            y--;
                        break;
                }

                BoardObject obj = game.Board.GetBoardObjectAt(x, y);

                if(obj != null)
                {
                    result.Detected = obj;
                    found = true;
                }
                else
                {
                    result.Distance++;
                }
            }

            return result;
        }

        internal class ScanResult
        {
            public BoardObject Detected;
            public byte Distance; //Amount of empty blocks between bot and Detected (not including bot or Detected)
        }
    }
}
