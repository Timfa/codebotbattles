﻿using CodeBotCore.Exceptions;
using CodeBotCore.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBotCore
{
    public class CodeBotBattlesGame
    {
        internal Board Board;

        public bool GameEnded
        {
            get
            {
                return GameResult.Status != GameResultObject.GameStatus.Ongoing;
            }
        }

        public GameResultObject GameResult
        {
            get
            {
                GameResultObject result = new GameResultObject();
                bool bottomHasPlayers = false, topHasPlayers = false, leftHasPlayers = false, rightHasPlayers = false;

                for(int i = 0; i < Board.boardObjects.Count; i++)
                {
                    if(Board.boardObjects[i] is Bot && !Board.boardObjects[i].Destroyed)
                    {
                        Bot bot = (Bot)Board.boardObjects[i];

                        switch(bot.Team)
                        {
                            case Bot.Teams.BOTTOM:
                                bottomHasPlayers = true;
                                break;
                            case Bot.Teams.LEFT:
                                leftHasPlayers = true;
                                break;
                            case Bot.Teams.RIGHT:
                                rightHasPlayers = true;
                                break;
                            case Bot.Teams.TOP:
                                topHasPlayers = true;
                                break;
                        }
                    }
                }

                int teamsAlive = 0;

                if (bottomHasPlayers)
                    teamsAlive++;

                if (topHasPlayers)
                    teamsAlive++;

                if (leftHasPlayers)
                    teamsAlive++;

                if (rightHasPlayers)
                    teamsAlive++;

                if (teamsAlive > 1)
                    result.Status = GameResultObject.GameStatus.Ongoing;

                if (teamsAlive == 1)
                {
                    result.Status = GameResultObject.GameStatus.Victory;

                    if (bottomHasPlayers)
                        result.WinningTeam = Bot.Teams.BOTTOM;

                    if (topHasPlayers)
                        result.WinningTeam = Bot.Teams.TOP;

                    if (leftHasPlayers)
                        result.WinningTeam = Bot.Teams.LEFT;

                    if (rightHasPlayers)
                        result.WinningTeam = Bot.Teams.RIGHT;
                }

                if (teamsAlive == 0)
                    result.Status = GameResultObject.GameStatus.Draw;

                return result;
            }
        }

        public CodeBotBattlesGame()
        {

        }

        public BoardObject[,] CurrentBoardState
        {
            get
            {
                if (Board == null)
                {
                    throw new BoardNotSetupException();
                }

                return Board.CurrentBoardState;
            }
        }

        public int BoardWidth
        {
            get
            {
                if (Board == null)
                {
                    throw new BoardNotSetupException();
                }

                return Board.Width;
            }
        }

        public int BoardHeight
        {
            get
            {
                if (Board == null)
                {
                    throw new BoardNotSetupException();
                }

                return Board.Height;
            }
        }

        public void StartNewGame()
        {
            if(Board == null)
            {
                throw new BoardNotSetupException();
            }

            Board.Reset();
        }

        public void SetupBoard(BattleMap map)
        {
            Board = new Board(this, map.Width, map.Height);

            for (int i = 0; i < map.Walls.Length; i++)
                new Wall(this, map.Walls[i].X, map.Walls[i].Y);

            for (int i = 0; i < map.DestructibleWalls.Length; i++)
                new DestructibleWall(this, map.DestructibleWalls[i].X, map.DestructibleWalls[i].Y);

            for(int i = 0; i < map.Bots.Length; i++)
            {
                Bot bot = new Bot(this, map.Bots[i].X, map.Bots[i].Y, map.Bots[i].Orientation, map.Bots[i].Team, map.Bots[i].MemorySize);
                bot.LoadScript(map.Bots[i].Script);
            }
            
            Board.Init();
        }

        public BoardObject GetObjectAtPosition(int x, int y)
        {
            if (Board == null)
            {
                throw new BoardNotSetupException();
            }

            return Board.GetBoardObjectAt(x, y);
        }

        public void GameTick()
        {
            if (Board == null)
            {
                throw new BoardNotSetupException();
            }

            Board.GameTick();
        }

        public class GameResultObject
        {
            public GameStatus Status = GameStatus.Ongoing;
            public Bot.Teams WinningTeam = Bot.Teams.BOTTOM;

            public enum GameStatus
            {
                Ongoing, Victory, Draw
            }
        }
    }
}
